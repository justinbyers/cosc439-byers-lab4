#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main() {

	int n;
	pid_t pid;

	printf("Enter an integer between 1-100\n");
	scanf("%d", &n);

	while(n < 1 || n > 100){
		printf("Enter # between 1-100:\n ");
		scanf("%d", &n);
		//n_arr[0] = &n;
	}

	printf("You entered: %d\n", n);
	pid = fork();

	if(pid == 0){ //child process
		char buffer [33]; // to hold the int to string variable 
		sprintf(buffer,  "%d", n); // convert int to string
		char *argv[] = {buffer, NULL}; // pass n (buffer = [str]n)
		char *envp[] = {"gcc", "collatz-edited.c", "-o","collatz-edited", NULL}; // environment params
		execve("collatz-edited", argv, envp);

	}
	else{ //parent process
		wait();
		printf("process ended\n");
	}

	return 0;
	
}


