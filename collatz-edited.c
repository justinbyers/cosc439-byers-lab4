#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>

int main(int argc, char *argv[], char *envp[]) {

	int n = atoi(argv[0]); // convert from string back to int
	pid_t pid;

	printf("START\n");

	pid = fork();

	if(pid == 0){ //child process
		while(n > 1){
			if(n % 2 == 0)
				n = n / 2;
			else
				n = (n * 3) + 1;
			printf("%d\n", n);
		}

	}
	else{ //parent process
		wait();
		printf("END\n");
	}
	return 0;
	
}


