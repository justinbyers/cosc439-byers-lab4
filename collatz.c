#include <stdio.h>
#include <sys/types.h>

int main() {

	int n;
	pid_t pid;

	printf("START\n");
	scanf("%d", &n);

	while(n < 1){
		printf("Enter valid positive #: ");
		scanf("%d", &n);
	}

	pid = fork();

	if(pid == 0){ //child process
		while(n > 1){
			if(n % 2 == 0)
				n = n / 2;
			else
				n = (n * 3) + 1;
			printf("%d\n", n);
		}

	}
	else{ //parent process
		wait();
		printf("END\n");
	}
	return 0;
	
}


