#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <stdlib.h>

void* printHelloThread(void* param){
	printf("Hello world! I am thread #%lu\n", pthread_self());
	pthread_exit(NULL);
	return NULL;
}

int main(int argc, char *argv[]){
	int n = atoi(argv[1]);
	
	printf("Creating %d threads...\n", n);

	while(n < 1 || n > 10){
		printf("Enter an integer (1-10):");
		scanf("%d", &n);
	}
	pthread_t thread[n];

	for(int i = 0; i < n; i ++){
		pthread_create(&(thread[i]), NULL, printHelloThread, NULL);
		printf("Creating thread #%lu...\n", (thread[i]));
	}

	for(int i = 0; i < n; i ++){
		pthread_join((thread[i]), NULL);
	}
	return 0;
}
